import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navigationbar';
import Caurosal from './components/caurosal';
import Aboutus from './components/aboutus';
import Admission from './components/admission';
import Subjects from './components/subjects';
import Contact from './components/contact';
import Footer from './components/footer';

function App() {
  return (
    <div>
      <Navbar />
      <Caurosal />
      <Aboutus />
      <Admission />
      <Subjects />
      <Contact />
      <Footer />
    </div>
  );
}

export default App;
