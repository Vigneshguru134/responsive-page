import React from 'react';
import {Container, Form, Button} from 'react-bootstrap';

export default function contact() {
  return (
    <div className="contact pb-5">
      <Container>
        <h2 style={{padding: '2rem'}}>Register Here</h2>
        <div className="row">
          {' '}
          <div className="register col-md-5 col-lg-6 ">
            <Form>
              <Form.Group>
                <Form.Label>Name</Form.Label>
                <Form.Control placeholder="Enter your name"></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>E-mail</Form.Label>
                <Form.Control placeholder="Enter your Email"></Form.Control>
              </Form.Group>

              <Form.Group>
                <Form.Label>Address</Form.Label>
                <Form.Control placeholder="Enter your Address"></Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Location</Form.Label>
                <Form.Control placeholder="Enter your Location"></Form.Control>
              </Form.Group>
              <Button className="p-3" style={{borderRadius: '1.8rem'}}>
                Register
              </Button>
            </Form>
          </div>
          <div className="info col-md-6 col-lg-6 ">
            <h4 className="py-4">Contact-us</h4>
            <div className="pl-4">
              <div>
                <h3>Address</h3>
                <p>No,3 chennai TamilNadu India</p>
              </div>
              <div>
                <h4>Location</h4>
                <p>Chennai</p>
              </div>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
