import React from 'react';
import img2 from '../assests/header.jpeg';
import '../App.css';

export default function aboutus() {
  return (
    <div id="about">
      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-md-6">
            <img src={img2} className="img-responsive aboutImage" alt="" />
          </div>
          <div className="col-xs-12 col-md-6">
            <div className="about-text">
              <h2>About Us</h2>
              <ul>
                <li>This is vignesh guru</li>
                <li>psgcollege of arts and science</li>
                <li>Located in coimbatore</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
