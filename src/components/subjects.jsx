import React from 'react';
import {Container} from 'react-bootstrap';
import '../App.css';
import img1 from '../assests/chem.svg';

const subjects = [
  {
    SubName: 'Maths',
    image: img1,
  },
  {
    SubName: 'Physics',
    image: img1,
  },
  {
    SubName: 'Chemistry',
    image: img1,
  },
  {
    SubName: 'Accountancy',
    image: img1,
  },
  {
    SubName: 'biology',
    image: img1,
  },
  {
    SubName: 'social',
    image: img1,
  },
];

export default function Subjects() {
  return (
    <div className="subjects">
      <h2>Products List</h2>
      <Container>
        <div className="row sub-list" style={{margin: 0}}>
          {subjects.map((names) => (
            <div className=" col-md-4 col-lg-4 col-xs-12 sub-name">
              <img src={names.image} />
              <h3>{names.SubName}</h3>
            </div>
          ))}
        </div>
      </Container>
    </div>
  );
}
