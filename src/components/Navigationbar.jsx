import React from 'react';
import {Navbar, NavDropdown, Nav} from 'react-bootstrap';

export default function Navigationbar() {
  return (
    <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
      <Navbar.Brand href="#home" style={{fontSize: '1.5rem'}}>
        Responsive Page
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav " />
      <Navbar.Collapse id="responsive-navbar-nav ">
        <Nav className="ml-auto pr-5" style={{fontSize: '1.5rem'}}>
          <Nav.Link href="#deets">About</Nav.Link>
          <Nav.Link href="#memes">Products</Nav.Link>
          <Nav.Link href="#memes">Testimonals</Nav.Link>
          <Nav.Link href="#memes">Contact</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
